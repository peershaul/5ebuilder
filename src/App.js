import "./App.css"
import Stages from "./Stages/Stages.js"

function App() {
  return (
    <div>
      <Stages />
    </div> 
  );
}

export default App;
