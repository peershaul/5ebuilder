import RaceStage from './RaceStage.js'
import ClassStage from './ClassStage.js';

const stages = [
	<RaceStage />,
	<ClassStage />
]

function StageInfo(elem){
	return(
		<div id = 'stage-infobox'>{stages[elem.props.stage]}</div>
	)
}

export default StageInfo;
