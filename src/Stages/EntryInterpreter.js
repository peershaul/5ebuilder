function class_detect(entry){
	if(typeof entry == 'string')
		return "text-only"
	else if(typeof entry == 'object')
		return entry.type
}

function output_html(entry, level){
	if(typeof entry == 'string')
		return <p>{entry}</p>
	else if(typeof entry == 'object'){
		if(entry.type == 'quote')
			return (
				<>
					{entry.entries.map(ent => <p>{ent}</p>)}
					<span>By {entry.by}</span>
				</>
			)
		else if(entry.type == 'entries')
			return(
				<>
					<h3 onClick={clicked_section}>{entry.name}<div></div></h3>
					{entry['entries'].map((sub_entry, index) => EntryInterpreter(sub_entry, index, level + 1))}
				</>
			)
		else if(entry.type == 'table'){
			return(
				<>
					<h4>TABLE</h4>
				</>
			)
		}
	}
}

export function clicked_section(event){
	event.target.closest('section').classList.toggle('collapsed')
} 

export function EntryInterpreter(entry, index, level = 0){
	return (
		<section 
			key={index} 
			level={level} 
			className={`entry-section ${class_detect(entry)} ${level % 2 == 0? 'even' : 'odd'}`}>
			{output_html(entry, level)}
		</section>
	)
}

export default EntryInterpreter;

