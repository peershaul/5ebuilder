

function AbilityScoreWidget(elem){
	return (
		<div className='ability-score-widget'><div>
				<span className='name'>{elem.props.name}</span>
				<span className={elem.props.value > 0? 'positive value' : 'negative value'}>{elem.props.value > 0? '+' : ''}{elem.props.value}</span>
		</div></div>
	)	
}

export default AbilityScoreWidget;
