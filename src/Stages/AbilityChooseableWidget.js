import './AbilityChooseableWidget.css'

function AbilityChoosableWidget(elem){
	const choose = elem.props.choose;	
	if(choose == null)
		return (<div style={{display: 'none'}}></div>);
	else{
		return (
			<div className='choosable-ability-container'>
				<span className='description'>
					{"you can choose to add "}
					<span className='positive amount'>+1</span>
					{" to "}
					<span className='count'>{choose.count}</span>
					{" of these abilities"}
				</span>
				<div className='abilities-list-container'>
					{choose.from.map((ability, index) => {
						return <div className='choosable-ability' key={index}><span>{ability}</span></div>	
					})}
				</div>
			</div>
		)
	}
}

export default AbilityChoosableWidget;
