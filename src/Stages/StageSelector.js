import React from 'react'
import "./StageSelector.css"

class StageSelector extends React.Component{

    button_clicked(key){
        this.props.props.stage_changed(key);
    }

    render(){
        let stages = [];
        for(let i = 0; i < this.props.props['stage_count']; i++){
            if(this.props.props['current_stage'] === i)
                stages.push(
									<div className="stage-button active" key={i}
                      onClick={() => this.button_clicked(i)}><div>
												<p>{i + 1}</p>
												<span>{this.props.props.labels[i]}</span>
									</div></div>)
            else 
                stages.push(
										<div className="stage-button" key={i}
													onClick={() => this.button_clicked(i)}><div>
															<p>{i + 1}</p>
															<span>{this.props.props.labels[i]}</span>
										</div></div>
								)
        }
        return(
            <div id='stage-button-container'>
                {stages}
            </div>
        )
    }
}




export default StageSelector;
