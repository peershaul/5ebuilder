import React from 'react'
import RaceSelector from './RaceSelector.js'

class RaceStage extends React.Component{

	constructor(elem){
		super(elem);
		
		this.state = {
			loaded_races: false,
			displayed_races: []
		}
		this.races = []	
	}

	componentDidMount = async () => {
		let data = await fetch('https://5etools-mirror-1.github.io/data/races.json')
												.then(response => response.json())	

		const races_data = data.race

		data = await fetch('https://5etools-mirror-1.github.io/data/fluff-races.json')
									.then(response => response.json())

		const fluff_data = data.raceFluff;
		
		for(let i = 0; i < races_data.length; i++){
			let is_duplicate = false;
			let duplicate_source = false;

			for(let j = this.races.length - 1; j >= 0; j--)
				if(this.races[j].name == races_data[i].name){
					
					for(let h = 0; h < this.races[j].sources.length; h++)
						if(this.races[j].sources[h] == races_data[i].source){
							duplicate_source = true;
							break;
						}

					is_duplicate = true;
					if(duplicate_source) break;

					this.races[j].sources.push(races_data[i].source);
					this.races[j].sourcePages.push(races_data[i]);
					if(races_data[i].hasFluff != undefined && races_data[i].hasFluff)
						this.races[j].fluffSources.push(races_data[i].source);

					if(races_data[i].hasFluffImages != undefined && races_data[i].hasFluffImages)
						this.races[j].fluffImageSources.push(races_data[i].source)

					break;
				}
			
			if(!is_duplicate && !duplicate_source){
				const hasFluff = races_data[i].hasFluff == undefined? false : races_data[i].hasFluff
				const hasFluffImages = races_data[i].hasFluffImages == undefined? false : races_data[i].hasFluffImages
				this.races.push({
					name: races_data[i].name,
					sources: [races_data[i].source],
					fluffSources: hasFluff? [races_data[i].source] : [],
					fluffImageSources: hasFluffImages? [races_data[i].source] : [], 
					fluffPages: [],
					sourcePages: [races_data[i]],
				})
			}
		}

		for(let i = 0; i < this.races.length; i++){
			for(let j = 0; j < this.races[i].fluffSources.length; j++)
				for(let h = 0; h < fluff_data.length; h++)
					if(this.races[i].fluffSources[j] == fluff_data[h].source && this.races[i].name == fluff_data[h].name){
						this.races[i].fluffPages.push(fluff_data[h]);
						break;
					}
		}
		this.setState({
			displayed_races: this.races.slice(),
			loaded_races: true
		})

		console.log(this.races);
	}

	searchFieldChanged = new_value => {
		if(new_value === '') this.setState({displayed_races: this.races.slice()}); 
		let displayed_races = []
		for(let i = 0; i < this.races.length; i++){
			if(this.races[i].name.toLowerCase().includes(new_value.toLowerCase()))
				displayed_races.push(this.races[i]);
			else
				for(let j = 0; j < this.races[i].sources.length; j++)
					if(this.races[i].sources[j].toLowerCase().includes(new_value.toLowerCase())){
						displayed_races.push(this.races[i]);
						break;
					}
		}
		this.setState({displayed_races: displayed_races.slice()})
	}

	render(){
		if(!this.state.loaded_races)
			return <div className='stagebox races loading'><div>Loading</div></div>
		else 
			return (
				<div className='stagebox races'>
					<RaceSelector props={{
						races: this.state.displayed_races,
						searchFieldChanged: this.searchFieldChanged
					}}/>	
				</div>
			)
	}	
} 

export default RaceStage;
