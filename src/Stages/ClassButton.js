import React from 'react'

function DrawPlaceholder(name, show){
	if(show)
		return(
			<span className='placeholder'>{name.charAt(0)}</span>
		)
	else
		return <span style={{display: 'none'}}></span>
}

function is_black_list(name){
	const black_list_names = [
		'Generic',
		'Mystic',
		'Rune scribe',
		'Sidekick'
	]

	for(let i = 0; i < black_list_names.length; i++)
		if(black_list_names[i] == name)
			return true;
	return false;
}

class ClassButton extends React.Component{

	constructor(props){
		super(props);
		this.state = {
			image_url: `url(/Classes/${this.props.props.class_name}.png)`
		}
	}

	render(){
		const props = this.props.props;
		const blacklisted = is_black_list(props.class_name)
		return(
			<div className={props.is_active? 'class-button active' : 'class-button'}>
				<div className={blacklisted? 'picture blacklisted' : 'picture'}
						 style={{backgroundImage: this.state.image_url}}
							onClick={() => props.clicked_event(props.index)}>
					{DrawPlaceholder(props.class_name, blacklisted)}
				</div>
				<div className='name'>{props.class_name}</div>
			</div> 	
		)
	}
}

export default ClassButton;
