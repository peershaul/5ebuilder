import React from 'react'

import RaceSelectBox from './RaceSelectBox.js';
import "./RaceSelector.css"

class RaceSelector extends React.Component{

	constructor(elem){
		super(elem);
		this.state = {
			search_field: '',
		}
	}
	
	handleSearchField = event => {
		const search_field = event.target.value;
		this.setState({search_field: search_field})	
		console.log(search_field);
		this.props.props.searchFieldChanged(search_field);	
	}

	render(){
		return (
			<div id='race-selector'>
				<div id='race-search-container'>
					<input onChange={this.handleSearchField} placeholder='search source or race' value={this.state.search_field}></input>	
					<span>found {this.props.props.races.length} races</span>
				</div>
				<div id='race-displayer'>
					{this.props.props.races.map((race, index) => {
						return <RaceSelectBox props={{race: race}} key={index} />	
					})}
				</div>
			</div>
		) 
	}
}

export default RaceSelector;
