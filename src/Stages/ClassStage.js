import React from 'react'
import ClassButton from './ClassButton';
import ClassInfo from './ClassInfo';

import './ClassStage.css'

function capitalize(text){
	const first_letter = text.charAt(0);
	const first_letter_cap = first_letter.toUpperCase();
	const remainingLetters = text.slice(1);
	const capitalized = first_letter_cap + remainingLetters;
	return capitalized.replace('-', ' ')
}

class ClassStage extends React.Component{
	constructor(elem){
		super(elem);
		this.class_names_cap = []
		this.class_names = []
		this.state = {
			loaded: false,
			selected_class: 10	
		}

		this.infoRef = React.createRef();
	}

	componentDidMount = async () => {
		const classesIndex = await fetch('https://5etools-mirror-1.github.io/data/class/index.json')
														.then(response => response.json())
		this.class_names = [];

		for(const name in classesIndex){
			let found = false;
			for(let i = this.class_names.length - 1; i >= 0; i--)
				if(this.class_names[i] == name){
					found = true;
					break;
				}
			if(!found) this.class_names.push(name)
		}
	
		this.class_names_cap = [];
		for(let i = 0; i < this.class_names.length; i++)
			this.class_names_cap.push(capitalize(this.class_names[i]))

		console.log(this.class_names)
		this.setState({loaded: true})
		if(this.infoRef.current != null)
			this.infoRef.current.change_class(this.state.selected_class == -1? null : this.class_names[this.state.selected_class])
	}

	class_clicked = index => {
		if(index != this.state.selected_class){	
			this.setState({selected_class: index})
			this.infoRef.current.change_class(this.class_names[index])
		}
	}

	render(){
		if(this.state.loaded)
			return(
				<div className='stagebox classes'>
					<div id='class-buttons-container'>
						{this.class_names_cap.map((name, index) => {
							return <ClassButton props={{
								class_name: name,
								index: index,
								is_active: index == this.state.selected_class, 
								clicked_event: this.class_clicked
							}} key={index} />		
						})}
					</div>
					<ClassInfo ref={this.infoRef}/>		
				</div>
			)
		else
			return(<h3>loading</h3>)
	}
}

export default ClassStage;
