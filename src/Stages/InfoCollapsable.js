import React from "react";
import './InfoCollapsable.css'

import {EntryInterpreter, clicked_section} from './EntryInterpreter.js'

class InfoCollapsable extends React.Component{
	render(){
		const fluff = this.props.fluff;

		if(fluff.entries == undefined || fluff.entries.length == 0)
			return null;

		return (
			<section className='info-collapsable'>
				<h2 onClick={clicked_section}>
					<span>
						{this.props.heading} 
						<span className='source'>
							<span className='book'>{fluff.source}</span>
							<span className='page'>P{fluff.page}</span>
						</span>
					</span>
					<div></div>
				</h2>
				{fluff.entries.map((entry, index) => EntryInterpreter(entry, index))}
			</section>
		)
	}
}

export default InfoCollapsable;


