import AbilityScoreWidget from "./AbilityScoreWidget";
import AbilityChoosableWidget from "./AbilityChooseableWidget";
import './AbilityScoreWidget.css'

function find_img(race){
	if(race.fluffImageSources.length == 0)
		return {};
	const image_source = race.fluffImageSources[0]; 	
	let image_page = null;
	for(let i = 0; i < race.fluffPages.length; i++)
		if(image_source == race.fluffPages[i].source)
			image_page = race.fluffPages[i];
	const image_path = 'https://5etools-mirror-1.github.io/img/' + image_page.images[0].href.path;
	return {
		backgroundImage: `url("${image_path}")`
	}
}

function get_card_description(entry_box, first_time){
	if(first_time && entry_box.entries == undefined)	
		return "Description not found";
	else if(typeof entry_box.entries[0] == 'object')
		return get_card_description(entry_box.entries[0], false);
	else if(typeof entry_box.entries[0] == 'string')
		return (entry_box.length > 1? entry_box.entries[1] : entry_box.entries[0]);
}

function description_finder(race){
	if(race.fluffSources.length == 0)
		return "Description not found";
	else 
		return get_card_description(race.fluffPages[0], true);
}

function get_ability_scores(race){
	for(let i = 0; i < race.sourcePages.length; i++)
		if(race.sourcePages[i].ability != undefined){
			const ability = race.sourcePages[i].ability;
			let names = [], values = [], choose = null;
			for(const name in ability[0]){
				if(name == 'choose'){
					choose = ability[0][name]
					console.log(race.name)
					console.log(choose)
				}
				else{
					names.push(name);
					values.push(ability[0][name]);
				}
			}
			if(choose != null){
				
			}
			return {
				choose: choose,
				req_abilities: names,
				req_values: values,
				source: race.sourcePages[i].source,
			}
		}
	return {source: "none"};
}

function draw_abilities(ability){
	if(ability.source == 'none')
		return <div style={{display: 'none'}}></div>;

	else{
		let ability_gui = [];
		if(ability.req_abilities != undefined && ability.req_abilities.length > 0)
			for(let i = 0; i < ability.req_abilities.length; i++){
				ability_gui.push(<AbilityScoreWidget props = {{
					name: ability.req_abilities[i],
					value: ability.req_values[i]
				}} key={i} />)
			}
		return(
			<div>
				<h5><span>ability modifiers <span>({ability.source})</span></span></h5>
				<div className='abilities-container'>
					<div className='normal-abilities-container'>
						{ability_gui}
					</div>
					<AbilityChoosableWidget props={{choose: ability.choose}} />					
				</div>
			</div>
		)
	}
}

function get_feats(race){
	const blacklist_feats = [
		'age',
		'alignment',
		'size',
		'language',
		'languages'
	]
	for(let i = 0; i < race.sourcePages.length; i++){
		const entries = race.sourcePages[i].entries;
		if(entries != undefined && entries.length > 0){
			let feats = [];
			for(let j = 0; j < entries.length; j++)
				if(typeof entries[j] == 'object'){
					let blacklisted = entries[j].name == undefined;
					for(let h = 0; h < blacklist_feats.length && !blacklisted; h++)
						if(blacklist_feats[h] == entries[j].name.toLowerCase())
							blacklisted = true;

					if(!blacklisted) feats.push(entries[j]);
				}
			if(feats.length > 0)
				return{
					source: race.sourcePages[i].source,
					feats: feats
				}
		}
	}
	return {
		source: 'none',
		feats: [],
	}
}

function draw_feats(feats){
	if(feats.source == 'none')
		return <div style={{display: 'none'}}></div>;
	else{
		return (
			<div>
				<h5><span>feats <span>({feats.source})</span></span></h5>
				<div className='feats-container'>
					{feats.feats.map((feat, index) => {
						return (
							<div className='feat'>
								<span>{feat.name}</span>
							</div>
						)	
					})}
				</div>
			</div>
		)	
	}
}

function RaceSelectBox(elem){
	const race = elem.props.race;
	const ability = get_ability_scores(race);
	const feats = get_feats(race);	
	return(
		<div 
			className = {race.fluffImageSources.length == 0? "race-selectbox no-image" : "race-selectbox"}>
			<div className='thumbnail' style={find_img(race)}></div>
			<div className='text-content'>
				<h3 className='name'>{race.name}</h3>
				<h5><span>summery <span>({race.fluffSources[0]})</span></span></h5>
				<p className='description'>{description_finder(race)}</p>
				<h5><span>sources</span></h5>
				<div className='sources-container'>
					{race.sources.map((source, index) => {
						return <div className={'source ' + source} key={index}>{source}</div>
					})}
				</div>
				{draw_abilities(ability)}
				{draw_feats(feats)}	
			</div>
		</div>	
	)
}

export default RaceSelectBox;
