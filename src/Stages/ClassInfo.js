import React from 'react'
import InfoCollapsable from './InfoCollapsable.js'

function draw_info(data){
	let fluffs_data = [];
	for(let i = 0; i < data.class.length; i++)
		if(data.class[i].fluff != undefined && (i == 0 || data.class[i].name == data.class[0].name))		
			for(let j = 0; j < data.class[i].fluff.length; j++)
				fluffs_data.push(data.class[i].fluff[j])
	
	let fluff_objs = [];
	for(let i = 0; i < fluffs_data.length; i++)
		fluff_objs.push(<InfoCollapsable key={i} fluff={fluffs_data[i]} heading="Info " />);
	return fluff_objs;
}

function ClassInfoSourceFilter(props){
		
}

class ClassInfo extends React.Component{

	constructor(props){
		super(props);	
		this.state = {
			loaded: false,
			selected_class: null,
			light_theme: false,
			class_sources: []
		}
		this.class_data_raw = null;
	}

	change_class = async new_class => {
		if(this.state.selected_class == new_class) return;
		this.setState({loaded: false, selected_class: new_class})	
		this.class_data_raw = await fetch(`https://5etools-mirror-1.github.io/data/class/class-${new_class}.json`)
																		.then(response => response.json())
		let sources = []
		for(let i = 0; i < this.class_data_raw.class.length; i++)
			sources.push(this.class_data_raw.class[i].source)

		this.setState({loaded: true, class_sources: sources})
	}	

	change_theme = () => {
		this.setState({light_theme: !this.state.light_theme})
	}

	render(){
		if(this.state.selected_class === null)
			return(
				<div id='class-infobox' className='empty'><span>There is no class selected, please select a class</span></div>
			)
		else if(!this.state.loaded)
			return(
				<div id='class-infobox' className='empty'><span>Loading...</span></div>
			)
		else 
			return(
				<div id='class-infobox' className={this.state.light_theme? 'light-mode' : ''}>
					<h1>{this.class_data_raw.class[0].name}</h1>
					<div className='content-container'>
						<div className='content'>
							{draw_info(this.class_data_raw)}
						</div>
					</div>
					<div className='buttons' onClick={this.change_theme}>
						<div className='dark-mode-button button'>
							<span className='gg-dark-mode'></span>
						</div>
					</div>
				</div>
			)
	}		
}

export default ClassInfo;
