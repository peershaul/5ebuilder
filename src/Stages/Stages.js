import React from 'react';

import "./StageSelector.js"
import StageSelector from './StageSelector.js';
import StageInfo from './StageInfo.js'

const labels = ['Race', 'Class', 'Speels', 'backstory'];

class Stages extends React.Component {
    
    constructor(props){
        super(props);

        this.state = {
            current_stage: 1
        }
    }

    stage_changed = new_stage => this.setState({current_stage: new_stage})

    render(){
        return (
            <div id="stages">
                <StageSelector props={{
										stage_count: labels.length, 
                    current_stage: this.state.current_stage, 
                    labels: labels,
                    stage_changed: this.stage_changed}} />
								<StageInfo props = {{stage: this.state.current_stage}}/>
            </div>
        ) 
    }
}

export default Stages;

